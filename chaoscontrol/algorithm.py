"""Contains implementation of evolutionary algorithm that let control of chaotic systems.
"""
# Author: Cristian Rios
# Email: cristianrios7@outlook.com
# Page: http://cristianrios.me

# pylint: disable=no-member, too-many-instance-attributes, too-many-public-methods, attribute-defined-outside-init

import math
import random
from collections import defaultdict

import chaoscontrol.utilities as utils


def search_call(func):
    """Function decorator to force call of search method for some attributes"""
    def attribute_exist(*args, **kwargs):
        try:
            value = func(*args, **kwargs)
        except AttributeError:
            msg = "Attribute requested doesn't exist. Please call search method before."
            raise AttributeError(msg)
        return value
    return attribute_exist


class Algorithm:
    """Implements an evolutionary algorithm that finds the k value for control a chaotic system.

    The control method used is the Positive Feedback Control. For a system x it's define as follow
        c = K*(x_(n-2)-x_(n-1))\n
        x_(n+1) = x_(n+1) + c

    This algorithm finds the k value used to apply modified Positive Feedback Control to a chaotic
    system.

    Every individual in population is a k value. It could be viewed as a chromosome with only one
    gen or a chromosome of length one.

    The algorithm try to keep the system stable only a small period of time given by `period` and
    don't include the delay system time.

    The evaluation function try to measure the distance between two virtual series, one of these
    is the serie shifted `orbit` times. If both series has the same period then the difference
    between them should be close to zero.

    The selection process is making by tournament.

    The reproduction proccess only has mutation. Crossing doesn't have sense in this context.
    Mutation is achieved adding some gaussian number.

    The algorithm keep the best individual in every run. The solution is the best of all.

    The population is constant, after every reproduction the new individual replace the old one.

    To help the algorithm convergency, a k interval is searching before the algorithm runs.

    Examples:
        >>> from chaoscontrol import (
                Algorithm,
                system_logistic_equation,
            )
        >>> options = {
                'individuals': 100,
                'generations': 100,
                'executions': 10,
                'period': 50,
                'orbits': 1,
            }
        >>> algorithm = Algorithm(system=system_logistic_equation, **options)
        >>> algorithm.search()
        >>> # You can also use the default defined values
        >>> from chaoscontrol import algorithm_options
        >>> algorithm = Algorithm(system=system_logistic_equation, **algorithm_options)

    Args:
        individuals (int): Population's size.
        generations (int): Amount of generations.
        executions (int): Amount of executions that algorithm will be executed.
        period (int): Amount of time in which system should keep stable.
        orbits (int): System's period desired.
        system (System): System to be controled.

    Attributes:
        _mating_pool (list[float]): Reproduction mating pool.
        _population (list[float]): Algorithm population.
        _min_k (float): Minimun k value.
        _max_k (float): Maximum k value.
        _index_best_individual (int): Keep the index of best individual in pupulation.
        _messages (dict): Contains information of algorithm execution.
        MSG_K_INTERVAL (str): Key for `get_messages`.
        MSG_PARTIAL_SOLUTIONS (str): Key for `get_messages`.
        MSG_NOT_SOLUTION (str): Key for `get_messages`.
        ARG_INDIVIDUALS (str): Key for kwargs.
        ARG_GENERATIONS (str): Key for kwargs.
        ARG_EXECUTIONS (str): Key for kwargs.
        ARG_PERIOD (str): Key for kwargs.
        ARG_ORBITS (str): Key for kwargs.
        ARG_SYSTEM (str): Key for kwargs.
        KEY_INDIVIDUAL (str): Key for `get_best_individual` and `get_population_with_evaluation`.
        KEY_EVALUATION (str): Key for `get_best_individual` and `get_population_with_evaluation`.
        KEY_MIN (str): Key for k min interval.
        KEY_MAX (str): Key for k max interval.

    Raises:
        ValueError: If some parameter is unknown or missing.
    """

    MSG_K_INTERVAL = 'k_interval'
    MSG_PARTIAL_SOLUTIONS = 'partial_solutions'
    MSG_NOT_SOLUTION = 'not_solution'

    KEY_INDIVIDUAL = 'k'
    KEY_EVALUATION = 'evaluation'
    KEY_MIN = 'min'
    KEY_MAX = 'max'

    ARG_INDIVIDUALS = 'individuals'
    ARG_GENERATIONS = 'generations'
    ARG_EXECUTIONS = 'executions'
    ARG_PERIOD = 'period'
    ARG_ORBITS = 'orbits'
    ARG_SYSTEM = 'system'
    _ARGS = (ARG_INDIVIDUALS, ARG_GENERATIONS, ARG_EXECUTIONS, ARG_PERIOD, ARG_ORBITS, ARG_SYSTEM)

    def __init__(self, **kwargs):
        self._validate_arguments(kwargs)
        for key, value in kwargs.items():
            setattr(self, utils.attrp(key), value)

    def _validate_arguments(self, kwargs):
        errors = defaultdict(list)
        utils.validate_unknown_arguments(self._ARGS, kwargs, errors)
        self._validate_required_arguments(kwargs, errors)
        self._validate_greater_arguments(kwargs, errors)
        if errors:
            raise ValueError(dict(errors))

    def _validate_required_arguments(self, kwargs, errors):
        for arg in self._ARGS:
            if arg not in kwargs.keys():
                errors[arg].append("Required argument")

    def _validate_greater_arguments(self, kwargs, errors):
        args = (
            self.ARG_INDIVIDUALS, self.ARG_GENERATIONS,
            self.ARG_EXECUTIONS, self.ARG_PERIOD, self.ARG_ORBITS
        )
        for arg in args:
            value = kwargs.get(arg, 0)
            if value < 1:
                errors[arg].append("Should be greater than 0")

    def _init_values(self):
        individuals = getattr(self, utils.attrp(self.ARG_INDIVIDUALS))
        self._population = [float('inf')]*individuals
        # the mating pool size is 10% aprox the amount of individuals
        self._mating_pool = [-1]*math.ceil(individuals*0.1)
        self._index_best_individual = -1
        self._evaluation_best_individual = float('inf')
        self._min_k = float('-inf')
        self._max_k = float('inf')
        self._messages = defaultdict(list)

    def search(self):
        """Execute the algorithm. Try to find k value.

        Returns:
            bool: `True` if k value was found and algorithm ends. `False` otherwise.
        """
        self._init_values()
        solution_found = False
        if self._find_k_interval() or self._find_k_interval_random():
            for _ in range(getattr(self, utils.attrp(self.ARG_EXECUTIONS))):
                self._build_population()
                for __ in range(getattr(self, utils.attrp(self.ARG_GENERATIONS))):
                    if not self._select():
                        # if some selection fails this execution fails and continue with next
                        break
                    self._reproduce()
                    # the replace of old individuals by new individuals is implicit
                # add partial solution to messages
                self._add_partial_solution_msg(
                    self._population[self._index_best_individual],
                    self._evaluation_best_individual
                )
            if self._evaluation_best_individual == float('inf'):
                self._add_solution_msg("Solution not found")
            else:
                solution_found = True
        else:
            self._add_solution_msg("K interval not found")
        return solution_found

    def _find_k_interval(self):
        step = 0.01
        top = 2
        bottom = -2

        min_k = bottom
        max_k = top
        eval_negative = self._evaluate(min_k)
        eval_positive = self._evaluate(max_k)

        while math.isinf(eval_negative) and min_k < 0:
            min_k = round(min_k + step, 2)
            eval_negative = self._evaluate(min_k)

        while math.isinf(eval_positive) and max_k > 0:
            max_k = round(max_k - step, 2)
            eval_positive = self._evaluate(max_k)

        if math.isinf(eval_negative) or math.isinf(eval_positive):
            found_k_interval = False
        else:
            found_k_interval = True
            self._min_k = round(min_k, 2)
            self._max_k = round(max_k, 2)
            self._add_k_interval_msg()

        return found_k_interval

    def _find_k_interval_random(self):
        found_k_interval = False

        top = 2
        bottom = -2
        mu = 0  # pylint: disable=invalid-name
        sigma = 0.02

        for _ in range(100):
            min_k = bottom
            max_k = top
            eval_negative = self._evaluate(min_k)
            eval_positive = self._evaluate(max_k)

            while math.isinf(eval_negative) and min_k < 0:
                min_k += math.fabs(random.gauss(mu, sigma))
                eval_negative = self._evaluate(min_k)

            while math.isinf(eval_positive) and max_k > 0:
                max_k -= math.fabs(random.gauss(mu, sigma))
                eval_positive = self._evaluate(max_k)

            if not math.isinf(eval_negative) and not math.isinf(eval_positive):
                found_k_interval = True
                self._min_k = round(min_k, 2)
                self._max_k = round(max_k, 2)
                self._add_k_interval_msg()
                break

        return found_k_interval

    def _evaluate(self, chromosome):
        system = getattr(self, utils.attrp(self.ARG_SYSTEM))
        system.set_k(chromosome)
        # if system iteration is inside delay period then there is not control, and when control
        # start, there is some fluctuations so, to be more accurate, we need to wait some
        # iterations to let the system settle before measure the distance
        period = getattr(self, utils.attrp(self.ARG_PERIOD))
        orbits = getattr(self, utils.attrp(self.ARG_ORBITS))
        delay = system.get_delay()*2
        x = system.get_serie(system.AXIS_X, delay + period + orbits)[delay:]   # pylint: disable=invalid-name
        diff = 0
        for i in range(period):
            diff += math.fabs(x[i + orbits] - x[i])
            if math.isnan(diff):
                # evaluation reach values too big or too small, this is not useful
                diff = float('inf')
                break
        return diff

    def _build_population(self):
        eval_best_individual = float('inf')
        mu = 0  # pylint: disable=invalid-name
        sigma = 1
        for i in range(getattr(self, utils.attrp(self.ARG_INDIVIDUALS))):
            k = random.gauss(mu, sigma)
            while k < self._min_k or k > self._max_k:
                k = random.gauss(mu, sigma)
            self._population[i] = k
            evaluation = self._evaluate(k)
            if evaluation < eval_best_individual:
                eval_best_individual = evaluation
                self._index_best_individual = i
                self._evaluation_best_individual = evaluation

    def _select(self):
        well_selected = True
        individuals = getattr(self, utils.attrp(self.ARG_INDIVIDUALS))
        size_tournament = math.ceil(individuals*0.03) # should be at least 1

        p = len(self._mating_pool)  # pylint: disable=invalid-name
        for i in range(p):
            eval_best_tournament = float('inf')

            tournament = 0
            lock = 0
            while tournament < size_tournament:
                index = random.randint(0, individuals-1)
                evaluation = self._evaluate(self._population[index])
                if not math.isinf(evaluation):
                    tournament += 1
                if evaluation < eval_best_tournament:
                    eval_best_tournament = evaluation
                    index_best_tournament = index

                # if population is so bad could happen that nobody win the tournament so we must
                # ensure the loop end
                lock += 1
                if lock > size_tournament*5:
                    index_best_tournament = -1
                    well_selected = False
                    break

            self._mating_pool[i] = index_best_tournament
        return well_selected

    def _reproduce(self):
        eval_best_individual = self._evaluate(self._population[self._index_best_individual])
        for index in self._mating_pool:
            if index != self._index_best_individual:
                # only apply mutation, crossing doesn't have sense in this context
                self._population[index] += random.gauss(0, 0.06)  # number between -0.2 and 0.2 aprx
                evaluation = self._evaluate(self._population[index])
                if evaluation < eval_best_individual:
                    eval_best_individual = evaluation
                    self._index_best_individual = index
                    self._evaluation_best_individual = evaluation

    def _add_k_interval_msg(self):
        msg = {self.KEY_MIN: self._min_k, self.KEY_MAX: self._max_k}
        self._messages[self.MSG_K_INTERVAL].append(msg)

    def _add_solution_msg(self, msg):
        self._messages[self.MSG_NOT_SOLUTION].append(msg)

    def _add_partial_solution_msg(self, k, evaluation):
        msg = {self.KEY_INDIVIDUAL: k, self.KEY_EVALUATION: evaluation}
        self._messages[self.MSG_PARTIAL_SOLUTIONS].append(msg)

    def get_individuals(self):
        """Get algorithm's individuals quantity.

        Returns:
            int: Individuals quantity.
        """
        return getattr(self, utils.attrp(self.ARG_INDIVIDUALS))

    def get_generations(self):
        """Get algorithm's generations quantity.

        Returns:
            int: Generations quantity.
        """
        return getattr(self, utils.attrp(self.ARG_GENERATIONS))

    def get_executions(self):
        """Get algorithm's executions quantity.

        Returns:
            int: Executions quantity.
        """
        return getattr(self, utils.attrp(self.ARG_EXECUTIONS))

    def get_period(self):
        """Get algorithm's period length.

        Returns:
            int: Period length.
        """
        return getattr(self, utils.attrp(self.ARG_PERIOD))

    def get_orbits(self):
        """Get algorithm's orbits quantity.

        Returns:
            int: Orbits quantity.
        """
        return getattr(self, utils.attrp(self.ARG_ORBITS))

    def get_system(self):
        """Get algorithm's system.

        Returns:
            System: System to control.
        """
        return getattr(self, utils.attrp(self.ARG_SYSTEM))

    @search_call
    def get_result(self):
        """Get algorithm's result.

        Returns:
            float: If search method retured True this will be the best k value found that let
            control the system. Defaults to `float('inf')`.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return self._population[self._index_best_individual]

    @search_call
    def get_messages(self):
        """Get algorithm's executions messages.

        Returns:
            dict: Contains executions messages like k interval, partial solutions and fail reasons.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return self._messages

    @search_call
    def get_population(self):
        """Get algorithm's population.

        Returns:
            list: All population individual's value.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return self._population

    @search_call
    def get_population_with_evaluation(self):
        """Get algorithm's population.

        Returns:
            list[dict]: All population individual's value and their evaluation.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return [
            {self.KEY_INDIVIDUAL: ind, self.KEY_EVALUATION: self._evaluate(ind)}
            for ind in self._population
        ]

    @search_call
    def get_max_k(self):
        """Get algorithm's max k value.

        Returns:
            float: Max value of k interval.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return self._max_k

    @search_call
    def get_min_k(self):
        """Get algorithm's min k value.

        Returns:
            float: Min value of k interval.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return self._min_k

    @search_call
    def get_best_individual(self):
        """Get algorithm's best individual.

        Returns:
            dict: Best individual's value and its evaluation.

        Raises:
            AttributeError: If search method haven't been called before.
        """
        return {
            self.KEY_INDIVIDUAL: self._population[self._index_best_individual],
            self.KEY_EVALUATION: self._evaluate(self._population[self._index_best_individual])
        }

    def set_individuals(self, individuals):
        """Set algorithm's individuals quantity.

        Args:
            individuals (int): Individuals quantity.
        """
        setattr(self, utils.attrp(self.ARG_INDIVIDUALS), individuals)

    def set_generations(self, generations):
        """Set algorithm's generations quantity.

        Args:
            generations(int): Generations quantity.
        """
        setattr(self, utils.attrp(self.ARG_GENERATIONS), generations)

    def set_executions(self, executions):
        """Set algorithm's executions quantity.

        Args:
            executions(int): Executions quantity.
        """
        setattr(self, utils.attrp(self.ARG_EXECUTIONS), executions)

    def set_period(self, period):
        """Set algorithm's period length.

        Args:
            period(int): Period length.
        """
        setattr(self, utils.attrp(self.ARG_PERIOD), period)

    def set_orbits(self, orbits):
        """Set algorithm's orbits quantity.

        Args:
            orbits(int): Orbits quantity.
        """
        setattr(self, utils.attrp(self.ARG_ORBITS), orbits)

    def set_system(self, system):
        """Set algorithm's system.

        Args:
            system(System): System to control.
        """
        setattr(self, utils.attrp(self.ARG_SYSTEM), system)

    @classmethod
    def clean_arguments(cls, options):
        """Filter from dict options all keys that Algorithm class doesn't accept.

        Args:
            options (dict):

        Returns:
            dict: options filtered.
        """
        return utils.clean_arguments(cls._ARGS, options)


options_algorithm = {
    Algorithm.ARG_INDIVIDUALS: 100,
    Algorithm.ARG_GENERATIONS: 100,
    Algorithm.ARG_EXECUTIONS: 1,
    Algorithm.ARG_PERIOD: 50,
    Algorithm.ARG_ORBITS: 1,
}
"""dict: Options to run the evolutionary algorithm.
"""
