"""Contains all clases, functions and objects related with chaotic systems.
"""
# Author: Cristian Rios
# Email: cristianrios7@outlook.com
# Page: http://cristianrios.me

# pylint: disable=invalid-name, too-many-public-methods

import random
import math
from collections import defaultdict

import chaoscontrol.utilities as utils


class System:
    """Chaotic system. Support multiple systems w/wc control and/or noise.

    The system's time serie depends of the system selected. If some value for K is given or setted
    the serie will have some control following the Positive Feedback Control method. If noise
    option is set to True the time series will have a Gaussian noise with the given mean and
    standard deviation or with default values.

    The following systems are supported.
        Logistic Equation
            x_(n+1) = a*x_n*(1-x_n)
        Henon System
            x_(n+1) = y_n - a*x_n^2 + 1\n
            y_(n+1) = b*x_n
        Tinkerbell System
            x_(n+1) = x_n^2 - y_n^2 + a*x_n + b*y_n\n
            y_(n+1) = 2*x_n*y_n + c*x_n + d*y_n
        Ikeda System
            x_(n+1) = a + b*(cos(u)*x_n - sin(u)*y_n)\n
            y_(n+1) = b*(sin(u)*x_n + cos(u)*y_n)\n
            u = (c-d) / (1 + x_n^2 + y_n^2))

    For a serie x given, the Positive Feedback Control is define as follow
        c = K*(x_(n-2)-x_(n-1))\n
        x_(n+1) = x_(n+1) + c

    Class System have attributes that represent the systems and their parameters. Also for noise
    and control values, and for some get methods that need right arguments. They should be used
    to build and use a System object.

    Examples:
        >>> from chaoscontrol import System
        >>> options = {
                System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
                System.ARG_X0: 0.1,
                System.ARG_A: 1,
                System.ARG_NOISE: True,
                System.ARG_NOISE_INTENSITY: 0.1,
                System.ARG_DELAY: 10,
            }
        >>> system = System(**options)
        >>> serie_x = system.get_serie(System.AXIS_X, 100)
        >>> # You can also use the default defined values
        >>> from chaoscontrol import options_logistic_equation
        >>> system = System(**options_logistic_equation)

    Attributes:
        SYSTEM_LOGISTIC_EQUATION (str)
        SYSTEM_HENON (str)
        SYSTEM_TINKERBELL (str)
        SYSTEM_IKEDA (str)
        ARG_NAME (str)
        ARG_X0 (str)
        ARG_Y0 (str)
        ARG_Z0 (str)
        ARG_A (str)
        ARG_B (str)
        ARG_C (str)
        ARG_D (str)
        ARG_NOISE (str)
        ARG_NOISE_INTENSITY (str)
        ARG_NOISE_MEAN (str)
        ARG_NOISE_STD (str)
        ARG_DELAY (str)
        ARG_K (str)
        ARG_N (str)
        AXIS_X (str)
        AXIS_Y (str)
        AXIS_Z (str)
        AXIS_X_WC (str)
        AXIS_Y_WC (str)
        AXIS_Z_WC (str)
        CONTROL_F (str)
        _dirty (bool): True if the serie should be calculated.

    Args:
        name (str): System's name. See supported systems.
        x0 (float): Initial system value in x axis.
        y0 (Optional[float]): Initial system value in y axis.
        z0 (Optional[float]): Initial system value in z axis.
        a (float): System parameter.
        b (Optional[float]): System parameter.
        c (Optional[float]): System parameter.
        d (Optional[float]): System parameter.
        noise (Optional[bool]): True if system will have noise, False otherwise. Defaults to False.
        noise_intensity (Optional[float]): Factor of noise, could amplified or reduce the noise.
            Required if `noise` is True.
        noise_mean (Optional[float]): Mean of gaussian noise. Defaults to 0.0.
        noise_std (Optional[float]): Standard deviation of gaussian noise. Defaults to 1.0.
        delay (int): Amount of iterations before control is applied. Should be greater than 1.
        k (Optional[float]): Control parameter. Defaults to 0.0.

    Raises:
        ValueError: If some parameter is unknown or missing.
    """

    SYSTEM_LOGISTIC_EQUATION = 'Logistic Equation'
    SYSTEM_HENON = 'Henon System'
    SYSTEM_TINKERBELL = 'Tinkerbell System'
    SYSTEM_IKEDA = 'Ikeda System'
    SYSTEM_LORENZ = 'Lorenz System'
    _SYSTEMS = (
        SYSTEM_LOGISTIC_EQUATION, SYSTEM_HENON, SYSTEM_TINKERBELL, SYSTEM_IKEDA, SYSTEM_LORENZ
    )

    ARG_NAME = 'name'
    ARG_X0 = 'x0'
    ARG_Y0 = 'y0'
    ARG_Z0 = 'z0'
    ARG_A = 'a'
    ARG_B = 'b'
    ARG_C = 'c'
    ARG_D = 'd'
    ARG_NOISE = 'noise'
    ARG_NOISE_INTENSITY = 'noise_intensity'
    ARG_NOISE_MEAN = 'noise_mean'
    ARG_NOISE_STD = 'noise_std'
    ARG_DELAY = 'delay'
    ARG_K = 'k'
    ARG_N = 'n'
    _ARGS = (
        ARG_NAME, ARG_X0, ARG_Y0, ARG_Z0, ARG_A, ARG_B, ARG_C, ARG_D, ARG_NOISE,
        ARG_NOISE_INTENSITY, ARG_NOISE_MEAN, ARG_NOISE_STD, ARG_DELAY, ARG_K, ARG_N
    )

    AXIS_X = 'x'
    AXIS_Y = 'y'
    AXIS_Z = 'z'
    AXIS_X_WC = 'x_wc'
    AXIS_Y_WC = 'y_wc'
    AXIS_Z_WC = 'z_wc'
    CONTROL_F = 'f'

    def __init__(self, **kwargs):
        self._validate_arguments(kwargs)
        for key, value in kwargs.items():
            setattr(self, utils.attrp(key), value)
        self._dirty = True

    def _validate_arguments(self, kwargs):
        errors = defaultdict(list)
        utils.validate_unknown_arguments(self._ARGS, kwargs, errors)
        self._validate_system(kwargs, errors)
        self._validate_initial_values(kwargs, errors)
        self._validate_parameters(kwargs, errors)
        self._validate_noise_values(kwargs, errors)
        self._validate_control_values(kwargs, errors)
        if errors:
            raise ValueError(dict(errors))

    def _validate_system(self, kwargs, errors):
        if self.ARG_NAME in kwargs:
            if kwargs[self.ARG_NAME] not in self._SYSTEMS:
                errors[self.ARG_NAME].append("Value not valid")
        else:
            self._add_error_argument_required(self.ARG_NAME, errors)

    def _validate_initial_values(self, kwargs, errors):
        if self.ARG_X0 not in kwargs:
            self._add_error_argument_required(self.ARG_X0, errors)

        name = kwargs.get(self.ARG_NAME, '')
        if self._is_system_two_axis(name) or self._is_system_three_axis(name):
            if self.ARG_Y0 not in kwargs:
                self._add_error_argument_required(self.ARG_Y0, errors)

        if self._is_system_three_axis(name):
            if self.ARG_Z0 not in kwargs:
                self._add_error_argument_required(self.ARG_Z0, errors)

    def _validate_parameters(self, kwargs, errors):
        if self.ARG_A not in kwargs:
            self._add_error_argument_required(self.ARG_A, errors)

        name = kwargs.get(self.ARG_NAME, '')
        if (self._is_system_two_parameter(name)
                or self._is_system_three_parameter(name)
                or self._is_system_four_parameter(name)):
            if self.ARG_B not in kwargs:
                self._add_error_argument_required(self.ARG_B, errors)

        if self._is_system_three_parameter(name) or self._is_system_four_parameter(name):
            if self.ARG_C not in kwargs:
                self._add_error_argument_required(self.ARG_C, errors)

        if self._is_system_four_parameter(name):
            if self.ARG_D not in kwargs:
                self._add_error_argument_required(self.ARG_D, errors)

    def _validate_control_values(self, kwargs, errors):
        if self.ARG_DELAY not in kwargs:
            self._add_error_argument_required(self.ARG_DELAY, errors)
        elif kwargs[self.ARG_DELAY] < 2:
            errors[self.ARG_DELAY].append("Should be greater than 1")

        if self.ARG_K not in kwargs:
            kwargs[self.ARG_K] = 0.0

    def _validate_noise_values(self, kwargs, errors):
        if self.ARG_NOISE not in kwargs:
            kwargs[self.ARG_NOISE] = False
        elif kwargs[self.ARG_NOISE]:
            if self.ARG_NOISE_INTENSITY not in kwargs:
                self._add_error_argument_required(self.ARG_NOISE_INTENSITY, errors)

        if self.ARG_NOISE_MEAN not in kwargs:
            kwargs[self.ARG_NOISE_MEAN] = 0.0

        if self.ARG_NOISE_STD not in kwargs:
            kwargs[self.ARG_NOISE_STD] = 1.0

    def _add_error_argument_required(self, arg, errors):  # pylint: disable=no-self-use
        errors[arg].append("Required argument")

    def _system_one_axis(self, function):
        n = getattr(self, utils.attrp(self.ARG_N))
        x0 = getattr(self, utils.attrp(self.ARG_X0))
        x, x_wc = self._system_axis_init(n, x0)
        f = [0]*n

        for i in range(1, n):
            x_wc[i] = function(x_wc, i)
            x[i] = function(x, i)
            self._add_noise_and_control(x, x_wc, f, i)

        self._system_set_values(x, x_wc, f)

    def _system_two_axis(self, function):
        n = getattr(self, utils.attrp(self.ARG_N))
        x0 = getattr(self, utils.attrp(self.ARG_X0))
        y0 = getattr(self, utils.attrp(self.ARG_Y0))
        x, x_wc = self._system_axis_init(n, x0)
        y, y_wc = self._system_axis_init(n, y0)
        f = [0]*n

        for i in range(1, n):
            x_wc[i], y_wc[i] = function(x_wc, y_wc, i)
            x[i], y[i] = function(x, y, i)
            self._add_noise_and_control(x, x_wc, f, i)

        self._system_set_values(x, x_wc, f, y, y_wc)

    def _system_three_axis(self, function):
        n = getattr(self, utils.attrp(self.ARG_N))
        x0 = getattr(self, utils.attrp(self.ARG_X0))
        y0 = getattr(self, utils.attrp(self.ARG_Y0))
        z0 = getattr(self, utils.attrp(self.ARG_Z0))
        x, x_wc = self._system_axis_init(n, x0)
        y, y_wc = self._system_axis_init(n, y0)
        z, z_wc = self._system_axis_init(n, z0)
        f = [0]*n

        for i in range(1, n):
            x_wc[i], y_wc[i], z_wc[i] = function(x_wc, y_wc, z_wc, i)
            x[i], y[i], z[i] = function(x, y, z, i)
            self._add_noise_and_control(x, x_wc, f, i)

        self._system_set_values(x, x_wc, f, y, y_wc, z, z_wc)

    def _add_noise_and_control(self, x, x_wc, f, i):
        if getattr(self, utils.attrp(self.ARG_NOISE)):
            r = self._get_noise()
            x_wc[i] += r
            x[i] += r

        control = 0
        if i > getattr(self, utils.attrp(self.ARG_DELAY)):
            control = self._get_control(x, i)
        x[i] += control
        f[i] = control

    def _system_axis_init(self, n, axis_0):  # pylint: disable=no-self-use
        axis_wc = [0]*n
        axis = [0]*n
        axis_wc[0] = axis_0
        axis[0] = axis_0
        return (axis, axis_wc)

    def _system_set_values(self, x, x_wc, f, y=None, y_wc=None, z=None, z_wc=None):  # pylint: disable=too-many-arguments
        setattr(self, utils.attrp(self.AXIS_X), x)
        setattr(self, utils.attrp(self.AXIS_X_WC), x_wc)
        setattr(self, utils.attrp(self.CONTROL_F), f)
        if y and y_wc:
            setattr(self, utils.attrp(self.AXIS_Y), y)
            setattr(self, utils.attrp(self.AXIS_Y_WC), y_wc)
        if z and z_wc:
            setattr(self, utils.attrp(self.AXIS_Z), z)
            setattr(self, utils.attrp(self.AXIS_Z_WC), z_wc)

    def _get_noise(self):
        mu = getattr(self, utils.attrp(self.ARG_NOISE_MEAN))
        sigma = getattr(self, utils.attrp(self.ARG_NOISE_STD))
        intensity = getattr(self, utils.attrp(self.ARG_NOISE_INTENSITY))
        noise = random.gauss(mu, sigma)*intensity
        return noise

    def _get_control(self, x, i):
        k = getattr(self, utils.attrp(self.ARG_K))
        control = k*(x[i-2] - x[i-1])
        return control

    def _logistic_equation_function(self, x, i):
        a = getattr(self, utils.attrp(self.ARG_A))
        return x[i-1]*(1-x[i-1])*a

    def _henon_function(self, x, y, i):
        a = getattr(self, utils.attrp(self.ARG_A))
        b = getattr(self, utils.attrp(self.ARG_B))
        return (y[i-1] - a*x[i-1]*x[i-1] + 1, b*x[i-1])

    def _tinkerbell_function(self, x, y, i):
        a = getattr(self, utils.attrp(self.ARG_A))
        b = getattr(self, utils.attrp(self.ARG_B))
        c = getattr(self, utils.attrp(self.ARG_C))
        d = getattr(self, utils.attrp(self.ARG_D))
        xi = x[i-1]*x[i-1] - y[i-1]*y[i-1] + a*x[i-1] + b*y[i-1]
        yi = 2*x[i-1]*y[i-1] + c*x[i-1] + d*y[i-1]
        return (xi, yi)

    def _ikeda_function(self, x, y, i):
        a = getattr(self, utils.attrp(self.ARG_A))
        b = getattr(self, utils.attrp(self.ARG_B))
        c = getattr(self, utils.attrp(self.ARG_C))
        d = getattr(self, utils.attrp(self.ARG_D))
        u = (c-d)/(1 + x[i-1]*x[i-1] + y[i-1]*y[i-1])
        xi = a + b*(x[i-1]*math.cos(u) - y[i-1]*math.sin(u))
        yi = b*(x[i-1]*math.sin(u) + y[i-1]*math.cos(u))
        return (xi, yi)

    def _lorenz_function(self):
        pass

    def _calculate_serie(self):
        if self._dirty:
            system = getattr(self, utils.attrp(self.ARG_NAME))
            if system == self.SYSTEM_LOGISTIC_EQUATION:
                self._system_one_axis(self._logistic_equation_function)
            elif system == self.SYSTEM_HENON:
                self._system_two_axis(self._henon_function)
            elif system == self.SYSTEM_TINKERBELL:
                self._system_two_axis(self._tinkerbell_function)
            elif system == self.SYSTEM_IKEDA:
                self._system_two_axis(self._ikeda_function)
            elif system == self.SYSTEM_LORENZ:
                self._system_three_axis(self._lorenz_function)
            self._dirty = False

    def _set_n(self, n):
        setattr(self, utils.attrp(self.ARG_N), n)
        self._dirty = True

    def _is_system_x(self, systems_set, name_default):
        is_system = False
        name = getattr(self, utils.attrp(self.ARG_NAME), name_default)
        if name in systems_set:
            is_system = True
        return is_system

    def _is_system_one_axis(self, name=None):
        # is exactly one axis
        systems_one = (self.SYSTEM_LOGISTIC_EQUATION,)
        return self._is_system_x(systems_one, name)

    def _is_system_two_axis(self, name=None):
        # is exactly two axis
        systems_two = (self.SYSTEM_HENON, self.SYSTEM_IKEDA, self.SYSTEM_TINKERBELL,)
        return self._is_system_x(systems_two, name)

    def _is_system_three_axis(self, name=None):
        # is exactly three axis
        systems_three = (self.SYSTEM_LORENZ,)
        return self._is_system_x(systems_three, name)

    def _is_system_one_parameter(self, name=None):
        # is exactly one parameter
        systems_one = (self.SYSTEM_LOGISTIC_EQUATION,)
        return self._is_system_x(systems_one, name)

    def _is_system_two_parameter(self, name=None):
        # is exactly two parameter
        systems_two = (self.SYSTEM_HENON,)
        return self._is_system_x(systems_two, name)

    def _is_system_three_parameter(self, name=None):
        # is exactly three parameter
        systems_three = (self.SYSTEM_LORENZ,)
        return self._is_system_x(systems_three, name)

    def _is_system_four_parameter(self, name=None):
        # is exactly four parameter
        systems_four = (self.SYSTEM_IKEDA, self.SYSTEM_TINKERBELL,)
        return self._is_system_x(systems_four, name)

    def _attribute_error(self, attr):
        raise AttributeError("{} has not attribute {}".format(self.get_name(), attr))

    def get_serie(self, axis, n):
        """Get one of system serie (depends of axis) of lenght n.

        Args:
            axis (str): Depend of the system. Axis are defined as class attributes and could be:\n
                AXIS_X, AXIS_Y, AXIS_Z (contains control).\n
                AXIS_X_WC, AXIS_Y_WC, AXIS_Z_WC (don't contain control).\n
                CONTROL_F (control force).
            n (int): Serie's lenght.

        Returns:
            list[float]: List of length n.

        Raises:
            ValueError: If `axis` is unknown or if system doesn't have the requested `axis`.
        """

        # calculate the serie occurs on demand. It is useful to avoid recalculate the serie if
        # nothing have changed or when we want the information of another axis or with length
        # less that before
        if getattr(self, utils.attrp(self.ARG_N), 0) < n:
            self._set_n(n)

        def _axis_error():
            raise ValueError("{}: Axis not valid".format(axis))

        if axis not in (
                self.AXIS_X, self.AXIS_Y, self.AXIS_Z,
                self.AXIS_X_WC, self.AXIS_Y_WC, self.AXIS_Z_WC,
                self.CONTROL_F
        ):
            _axis_error()
        if self._is_system_one_axis():
            if axis in (self.AXIS_Y, self.AXIS_Y_WC, self.AXIS_Z, self.AXIS_Z_WC):
                _axis_error()
        if self._is_system_two_axis():
            if axis in (self.AXIS_Z, self.AXIS_Z_WC):
                _axis_error()

        self._calculate_serie()
        return getattr(self, utils.attrp(axis))[:n]

    def get_delay(self):
        """Get serie's delay.

        Returns:
            int:
        """
        return getattr(self, utils.attrp(self.ARG_DELAY))

    def get_name(self):
        """Get serie's name.

        Returns:
            str:
        """
        return getattr(self, utils.attrp(self.ARG_NAME))

    def get_x0(self):
        """Get system's x0.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_X0))

    def get_y0(self):
        """Get system's y0.

        Returns:
            float:
        """
        if self._is_system_one_axis():
            self._attribute_error(self.ARG_Y0)
        return getattr(self, utils.attrp(self.ARG_Y0))

    def get_z0(self):
        """Get system's z0.

        Returns:
            float:
        """
        if self._is_system_one_axis() or self._is_system_two_axis():
            self._attribute_error(self.ARG_Z0)
        return getattr(self, utils.attrp(self.ARG_Z0))

    def get_a(self):
        """Get system's a parameter.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_A))

    def get_b(self):
        """Get system's b parameter.

        Returns:
            float:
        """
        if self._is_system_one_parameter():
            self._attribute_error(self.ARG_B)
        return getattr(self, utils.attrp(self.ARG_B))

    def get_c(self):
        """Get system's c parameter.

        Returns:
            float:
        """
        if self._is_system_one_parameter() or self._is_system_two_parameter():
            self._attribute_error(self.ARG_C)
        return getattr(self, utils.attrp(self.ARG_C))

    def get_d(self):
        """Get system's d parameter.

        Returns:
            float:
        """
        if (self._is_system_one_parameter()
                or self._is_system_two_parameter()
                or self._is_system_three_parameter()):
            self._attribute_error(self.ARG_D)
        return getattr(self, utils.attrp(self.ARG_D))

    def get_noise(self):
        """Get system's noise.

        Returns:
            bool:
        """
        return getattr(self, utils.attrp(self.ARG_NOISE))

    def get_noise_intensity(self):
        """Get system's noise intensity.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_NOISE_INTENSITY), 0)

    def get_noise_mean(self):
        """Get system's noise mean.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_NOISE_MEAN))

    def get_noise_std(self):
        """Get system's noise std.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_NOISE_STD))

    def get_k(self):
        """Get system's k.

        Returns:
            float:
        """
        return getattr(self, utils.attrp(self.ARG_K))

    def set_x0(self, x0):
        """Set system's x0.

        Args:
            x0(float):
        """
        setattr(self, utils.attrp(self.ARG_X0), x0)

    def set_y0(self, y0):
        """Set system's y0.

        Args:
            y0(float):
        """
        if self._is_system_one_axis():
            self._attribute_error(self.ARG_Y0)
        setattr(self, utils.attrp(self.ARG_Y0), y0)

    def set_z0(self, z0):
        """Set system's z0.

        Args:
            z0(float):
        """
        if self._is_system_one_axis() or self._is_system_two_axis():
            self._attribute_error(self.ARG_Z0)
        setattr(self, utils.attrp(self.ARG_Z0), z0)

    def set_a(self, a):
        """Set system's a parameter.

        Args:
            a(float):
        """
        setattr(self, utils.attrp(self.ARG_A), a)

    def set_b(self, b):
        """Set system's b parameter.

        Args:
            b(float):
        """
        if self._is_system_one_parameter():
            self._attribute_error(self.ARG_B)
        setattr(self, utils.attrp(self.ARG_B), b)

    def set_c(self, c):
        """Set system's c parameter.

        Args:
            c(float):
        """
        if self._is_system_one_parameter() or self._is_system_two_parameter():
            self._attribute_error(self.ARG_C)
        setattr(self, utils.attrp(self.ARG_C), c)

    def set_d(self, d):
        """Set system's d parameter.

        Args:
            d(float):
        """
        if (self._is_system_one_parameter()
                or self._is_system_two_parameter()
                or self._is_system_one_parameter()):
            self._attribute_error(self.ARG_D)
        setattr(self, utils.attrp(self.ARG_D), d)

    def set_noise(self, noise):
        """Set system's noise.

        Args:
            noise(bool):
        """
        setattr(self, utils.attrp(self.ARG_NOISE), noise)
        if noise and not self.get_noise_intensity():
            self.set_noise_intensity(0)

    def set_noise_intensity(self, noise_intensity):
        """Set system's noise intensity.

        Args:
            noise_intensity(float):
        """
        setattr(self, utils.attrp(self.ARG_NOISE_INTENSITY), noise_intensity)

    def set_noise_mean(self, noise_mean):
        """Set system's noise mean.

        Args:
            noise_mean(float):
        """
        setattr(self, utils.attrp(self.ARG_NOISE_MEAN), noise_mean)

    def set_noise_std(self, noise_std):
        """Set system's noise std.

        Args:
            noise_std(float):
        """
        setattr(self, utils.attrp(self.ARG_NOISE_STD), noise_std)

    def set_k(self, k):
        """Set the k control value.

        Args:
            k (float): Control parameter.
        """
        setattr(self, utils.attrp(self.ARG_K), k)
        self._dirty = True

    @classmethod
    def clean_arguments(cls, options):
        """Filter from dict options all keys that System class doesn't accept.

        Args:
            options (dict):

        Returns:
            dict: options filtered.
        """
        return utils.clean_arguments(cls._ARGS, options)


_default_options = {
    System.ARG_NOISE: False,
    System.ARG_NOISE_INTENSITY: 0.01,
    System.ARG_NOISE_MEAN: 0,
    System.ARG_NOISE_STD: 0.5,
    System.ARG_DELAY: 10,
    System.ARG_K: 0,
    System.ARG_N: 100,
}


options_logistic_equation = {
    System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
    System.ARG_X0: 0.1,
    System.ARG_A: 3.9,
}
"""dict: Options to build a Logistic Equation system.
"""
options_logistic_equation.update(_default_options)
system_logistic_equation = System(**options_logistic_equation)
"""System: Represent a Logistic Equation.
"""


options_henon_system = {
    System.ARG_NAME: System.SYSTEM_HENON,
    System.ARG_X0: -0.1,
    System.ARG_Y0: 0.1,
    System.ARG_A: 1.4,
    System.ARG_B: 0.3,
}
"""dict: Options to build a Henon system.
"""
options_henon_system.update(_default_options)
system_henon = System(**options_henon_system)
"""System: Represent a Henon system.
"""


options_ikeda_system = {
    System.ARG_NAME: System.SYSTEM_IKEDA,
    System.ARG_X0: 0.1,
    System.ARG_Y0: 0.1,
    System.ARG_A: 1,
    System.ARG_B: 0.9,
    System.ARG_C: 0.4,
    System.ARG_D: 0.6,
}
"""dict: Options to build an Ikeda system.
"""
options_ikeda_system.update(_default_options)
system_ikeda = System(**options_ikeda_system)
"""System: Represent an Ikeda system.
"""


options_tinkerbell_system = {
    System.ARG_NAME: System.SYSTEM_TINKERBELL,
    System.ARG_X0: 0.1,
    System.ARG_Y0: 0.1,
    System.ARG_A: 0.5,
    System.ARG_B: -0.6,
    System.ARG_C: 2.2,
    System.ARG_D: 0.5,
}
"""dict: Options to build a Tinkerbell system.
"""
options_tinkerbell_system.update(_default_options)
system_tinkerbell = System(**options_tinkerbell_system)
"""System: Repesent a Tinkerbell system.
"""


all_systems = [
    system_logistic_equation,
    system_henon,
    system_ikeda,
    system_tinkerbell
]
"""list[System]: List of all available systems.
"""


all_systems_options = [
    options_logistic_equation,
    options_henon_system,
    options_ikeda_system,
    options_tinkerbell_system
]
"""list[dict]: List af all available system options.
"""
