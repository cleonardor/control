echo "Building docs sources"
sphinx-apidoc -f -o docs/source chaoscontrol

echo "Building docs in html"
cd docs
sphinx-build -M html source/ build/