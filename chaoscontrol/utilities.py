"""Utility functions
"""


def clean_arguments(args, options):
    """Filter from dict options all keys that are not in args.

    Args:
        args (tuple): Valid keys.
        options (dict): Options to be validated.

    Returns:
        dict: options filtered.
    """
    options_ = {}
    for key, val in options.items():
        if key in args:
            options_[key] = val
    return options_


def validate_unknown_arguments(known_args, kwargs, errors):
    """Validate all keys of kwargs against known_args, add an error if it's not valid.

    Args:
        known_args (tuple): Valid keys.
        kwargs (dict): Options to be validated.
        errors: (defauldict(list)): Where error will be added if exist.
    """
    for arg in kwargs:
        if arg not in known_args:
            errors[arg].append("Argument not valid")

def attrp(attr):
    """Change an attribute name to private
    """
    return "_{}".format(attr)
