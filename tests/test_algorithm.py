import pytest
from unittest.mock import patch, MagicMock

from chaoscontrol.algorithm import Algorithm
from chaoscontrol.systems import system_logistic_equation


def inf_list(n):
    return [float('inf')]*n


@pytest.fixture
def algorithm_arguments():
    kwargs = {
        'individuals': 10,
        'generations': 1,
        'executions': 1,
        'period': 50,
        'orbits': 1,
        'system': None,
    }
    return kwargs

def test_missing_argument(algorithm_arguments):
    del algorithm_arguments['system']
    with pytest.raises(ValueError) as excinfo:
        Algorithm(**algorithm_arguments)
    assert excinfo.value.args[0] == {'system': ['Required argument']}


def test_missing_arguments(algorithm_arguments):
    del algorithm_arguments['orbits']
    del algorithm_arguments['system']
    with pytest.raises(ValueError) as excinfo:
        Algorithm(**algorithm_arguments)
    assert excinfo.value.args[0] == {
        'orbits': ['Required argument', 'Should be greater than 0'],
        'system': ['Required argument']
    }


def test_invalid_arguments(algorithm_arguments):
    algorithm_arguments['unknown'] = 'unknown'
    with pytest.raises(ValueError) as excinfo:
        Algorithm(**algorithm_arguments)
    assert excinfo.value.args[0] == {'unknown': ['Argument not valid']}


def test_find_k_interval_only_kmin_found(algorithm_arguments):
    with patch.object(Algorithm, '_evaluate') as evaluate_mock:
        evaluate_mock.side_effect = inf_list(2) + [1] + inf_list(200)
        alg = Algorithm(**algorithm_arguments)
        assert not alg._find_k_interval()


def test_find_k_interval_only_kmax_found(algorithm_arguments):
    with patch.object(Algorithm, '_evaluate') as evaluate_mock:
        evaluate_mock.side_effect = inf_list(202) + [1]
        alg = Algorithm(**algorithm_arguments)
        assert not alg._find_k_interval()


def test_find_k_interval_not_found(algorithm_arguments):
    with patch.object(Algorithm, '_evaluate') as evaluate_mock:
        evaluate_mock.side_effect = inf_list(402)
        alg = Algorithm(**algorithm_arguments)
        alg._init_values()
        assert not alg._find_k_interval()


def test_find_k_interval_found(algorithm_arguments):
    with patch.object(Algorithm, '_evaluate') as evaluate_mock:
        evaluate_mock.side_effect = inf_list(2) + [1, 1]
        alg = Algorithm(**algorithm_arguments)
        alg._init_values()
        assert alg._find_k_interval()


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
@patch('chaoscontrol.algorithm.random.gauss')
def test_find_k_interval_random_only_kmin_found(gauss_mock, evaluate_mock, algorithm_arguments):
    gauss_mock.side_effect = lambda mu, sigma: 2
    evaluate_mock.side_effect = inf_list(398) + [1, float('inf')]

    alg = Algorithm(**algorithm_arguments)
    assert not alg._find_k_interval_random()


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
@patch('chaoscontrol.algorithm.random.gauss')
def test_find_k_interval_random_only_kmax_found(gauss_mock, evaluate_mock, algorithm_arguments):
    gauss_mock.side_effect = lambda mu, sigma: 2
    evaluate_mock.side_effect = inf_list(398) + [float('inf'), 1]

    alg = Algorithm(**algorithm_arguments)
    assert not alg._find_k_interval_random()


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
@patch('chaoscontrol.algorithm.random.gauss')
def test_find_k_interval_random_not_found(gauss_mock, evaluate_mock, algorithm_arguments):
    gauss_mock.side_effect = lambda mu, sigma: 2
    evaluate_mock.side_effect = inf_list(400)

    alg = Algorithm(**algorithm_arguments)
    assert not alg._find_k_interval_random()


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
@patch('chaoscontrol.algorithm.random.gauss')
def test_find_k_interval_random_found(gauss_mock, evaluate_mock, algorithm_arguments):
    gauss_mock.side_effect = lambda mu, sigma: 2
    evaluate_mock.side_effect = inf_list(2) + [1, 1]

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    assert alg._find_k_interval_random()


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_build_population_and_best_initvidual(evaluate_mock, algorithm_arguments):
    individuals = 10
    index_best_individual = 6
    evaluate_result = [2]*individuals
    evaluate_result[index_best_individual] = 1
    evaluate_mock.side_effect = evaluate_result

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._min_k = -2
    alg._max_k = 2
    alg._build_population()
    assert len(alg._population) == individuals
    assert alg._index_best_individual == index_best_individual


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
@patch('chaoscontrol.algorithm.random.gauss')
def test_build_population_out_k_interval(gauss_mock, evaluate_mock, algorithm_arguments):
    individuals = 10
    evaluate_result = [2]*(individuals + 2)
    evaluate_mock.side_effect = evaluate_result
    gauss_result = [-3, 3] + [0]*individuals
    gauss_mock.side_effect = gauss_result

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._min_k = -2
    alg._max_k = 2
    alg._build_population()
    assert len(alg._population) == individuals


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_select_right_size_mating_pool(evaluate_mock, algorithm_arguments):
    individuals = 100
    evaluate_mock.side_effect = [1]*individuals
    algorithm_arguments['individuals'] = individuals

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._population = [0]*individuals
    # the initial values of mating_pool are -1, if the select process was ok all elements of
    # mating_pool should be positive (index of individuals in population)
    assert alg._select()
    assert not [a for a in alg._mating_pool if a < 0]


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_select_right_mating_pool_lock(evaluate_mock, algorithm_arguments):
    evaluate_mock.side_effect = inf_list(50)

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._population = [0]*10
    # if something was wrong method return false and mating_pool will have some -1
    assert not alg._select()
    assert [a for a in alg._mating_pool if a < 0]


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_reproduce_mating_pool_doesnt_have_best(evaluate_mock, algorithm_arguments):
    individuals = 10
    evaluate_mock.side_effect = [1]*(individuals+1)
    index_best = 3

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._population = [0]*individuals
    alg._index_best_individual = index_best
    alg._mating_pool = [0, 5, 8]
    # all individuals has de same evaluation, the best index doesnt change and all individuals in
    # mating pool are different of 0
    alg._reproduce()
    assert alg._index_best_individual == index_best
    for i in alg._mating_pool:
        assert alg._population[i] != 0
    for i in range(len(alg._population)):
        if i not in alg._mating_pool:
            assert alg._population[i] == 0


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_reproduce_mating_pool_has_best(evaluate_mock, algorithm_arguments):
    individuals = 10
    evaluate_mock.side_effect = [1]*(individuals+1)
    index_best = 3

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._population = [0]*individuals
    alg._index_best_individual = index_best
    alg._mating_pool = [0, index_best, 8]
    # all individuals has de same evaluation, the best index doesnt change and all individuals
    # in mating pool are different of 0 except the best
    alg._reproduce()
    assert alg._index_best_individual == index_best
    assert alg._population[0] != 0
    assert alg._population[8] != 0
    for i in range(len(alg._population)):
        if i not in [0, 8]:
            assert alg._population[i] == 0


@patch('chaoscontrol.algorithm.Algorithm._evaluate')
def test_reproduce_best_improve(evaluate_mock, algorithm_arguments):
    individuals = 10
    evaluate_mock.side_effect = [2] + [1]*individuals
    index_best = 3

    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._population = [0]*individuals
    alg._index_best_individual = index_best
    alg._mating_pool = [0, 5, 8]
    # all individuals has de same evaluation except the best, the best index change and all
    # individuals in mating pool are different of 0
    alg._reproduce()
    assert alg._index_best_individual == 0  # the first index in mating pool
    for i in alg._mating_pool:
        assert alg._population[i] != 0
    for i in range(len(alg._population)):
        if i not in alg._mating_pool:
            assert alg._population[i] == 0


def test_evaluate(algorithm_arguments):
    system = MagicMock()
    system.get_serie.side_effect = lambda axis, n: [1]*n
    system.get_delay.return_value = 0
    algorithm_arguments['system'] = system

    alg = Algorithm(**algorithm_arguments)
    # the serie only contains 1, so the difference with whatever orbit should be 0
    assert alg._evaluate(1) == 0


def test_search_not_found_k_interval_random(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg._find_k_interval = MagicMock(return_value=False)
    alg._find_k_interval_random = MagicMock(return_value=False)
    assert not alg.search()


def test_search_not_solution(algorithm_arguments):
    generations = 5
    executions = 20
    algorithm_arguments['generations'] = generations
    algorithm_arguments['executions'] = executions
    alg = Algorithm(**algorithm_arguments)
    alg._index_best_individual = 0
    alg._find_k_interval = MagicMock(return_value=True)
    alg._build_population = MagicMock()
    alg._select = MagicMock()
    alg._reproduce = MagicMock()
    alg._evaluate = MagicMock()

    assert not alg.search()  # solution is inf
    assert alg._find_k_interval.call_count == 1
    assert alg._build_population.call_count == executions
    assert alg._select.call_count == executions*generations
    assert alg._reproduce.call_count == executions*generations
    # _evaluate is called in build_population and reproduce
    assert alg._evaluate.call_count == 0


def test_search_solution(algorithm_arguments):
    generations = 5
    executions = 20
    algorithm_arguments['generations'] = generations
    algorithm_arguments['executions'] = executions
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._index_best_individual = 0
    alg._evaluation_best_individual = 1  # is not inf, so solution exist
    alg._population[0] = 1  # this will be the solution
    alg._init_values = MagicMock()
    alg._find_k_interval = MagicMock(return_value=True)
    alg._build_population = MagicMock()
    alg._select = MagicMock()
    alg._reproduce = MagicMock()
    alg._evaluate = MagicMock()

    assert alg.search()
    # _evaluate is called in build_population and reproduce
    assert alg._evaluate.call_count == 0
    assert alg._find_k_interval.call_count == 1
    assert alg._build_population.call_count == executions
    assert alg._select.call_count == executions*generations
    assert alg._reproduce.call_count == executions*generations


def test_search_not_solution_not_select(algorithm_arguments):
    # if all selections fails, solution will be the best of initial population
    # here build_population is a mock so solution is by default inf
    generations = 5
    executions = 20
    algorithm_arguments['generations'] = generations
    algorithm_arguments['executions'] = executions
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._index_best_individual = 0
    alg._find_k_interval = MagicMock(return_value=True)
    alg._build_population = MagicMock()
    alg._select = MagicMock(return_value=False)
    alg._reproduce = MagicMock()
    alg._evaluate = MagicMock()

    assert not alg.search()
    assert alg._find_k_interval.call_count == 1
    assert alg._build_population.call_count == executions
    assert alg._select.call_count == executions
    assert alg._reproduce.call_count == 0
    assert alg._evaluate.call_count == 0


def test_get_result_default(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    assert alg.get_result() == float('inf')


def test_get_result(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    alg._index_best_individual = 0
    alg._population[0] = 1
    assert alg.get_result() == 1


def test_get_attribute_without_call_search_method(algorithm_arguments):
    # search method call _init_values method
    alg = Algorithm(**algorithm_arguments)
    with pytest.raises(AttributeError) as execinfo:
        alg.get_result()
    msg = "Attribute requested doesn't exist. Please call search method before."
    assert execinfo.value.args[0] == msg


def test_validate_attributes_greater_than_zero(algorithm_arguments):
    algorithm_arguments[Algorithm.ARG_INDIVIDUALS] = 0
    with pytest.raises(ValueError) as execinfo:
        Algorithm(**algorithm_arguments)
    assert execinfo.value.args[0] == {'individuals': ['Should be greater than 0']}

 
def test_get_individuals(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_individuals() == 10


def test_get_generations(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_generations() == 1


def test_get_executions(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_executions() == 1


def test_get_period(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_period() == 50


def test_get_orbits(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_orbits() == 1


def test_get_system(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    assert alg.get_system() == None


def test_get_min_k(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    assert alg.get_min_k() == float('-inf')


def test_get_max_k(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg._init_values()
    assert alg.get_max_k() == float('inf')


def test_get_population(algorithm_arguments):
    algorithm_arguments['system'] = system_logistic_equation
    alg = Algorithm(**algorithm_arguments)
    alg.search()
    population = alg.get_population()
    assert len(population) == algorithm_arguments[Algorithm.ARG_INDIVIDUALS]
    assert isinstance(population[0], float)


def test_get_population_with_evaluation(algorithm_arguments):
    algorithm_arguments['system'] = system_logistic_equation
    alg = Algorithm(**algorithm_arguments)
    alg.search()
    population = alg.get_population_with_evaluation()
    first = population[0]
    assert len(population) == algorithm_arguments[Algorithm.ARG_INDIVIDUALS]
    assert isinstance(first, dict)
    assert len(first.keys()) == 2


def test_get_best_individual(algorithm_arguments):
    algorithm_arguments['system'] = system_logistic_equation
    alg = Algorithm(**algorithm_arguments)
    alg.search()
    best = alg.get_best_individual()
    assert isinstance(best, dict)
    assert len(best.keys()) == 2


def test_set_individuals(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_individuals(20)
    assert alg.get_individuals() == 20


def test_set_generations(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_generations(10)
    assert alg.get_generations() == 10


def test_set_executions(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_executions(10)
    assert alg.get_executions() == 10


def test_set_period(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_period(100)
    assert alg.get_period() == 100


def test_set_orbits(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_orbits(3)
    assert alg.get_orbits() == 3


def test_set_system(algorithm_arguments):
    alg = Algorithm(**algorithm_arguments)
    alg.set_system(system_logistic_equation)
    assert alg.get_system() == system_logistic_equation
