"""Contains utility functions to build systems and find their control values using evolutionary
algorithms

    Examples:
        >>> from chaoscontrol import (
                Algorithm,
                System,
                get_control_and_system,
            )
        >>> options = {
                Algorithm.ARG_INDIVIDUALS: 10,
                Algorithm.ARG_GENERATIONS: 1,
                Algorithm.ARG_EXECUTIONS: 1,
                Algorithm.ARG_PERIOD: 50,
                Algorithm.ARG_ORBITS: 1,
                System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
                System.ARG_X0: 0.1,
                System.ARG_A: 1,
                System.ARG_DELAY: 10,
            }
        >>> get_control_and_system(options)  # utility function
"""
# Author: Cristian Rios
# Email: cristianrios7@outlook.com
# Page: http://cristianrios.me

from collections import defaultdict

from chaoscontrol.systems import System
from chaoscontrol.algorithm import Algorithm

STATUS = 'status'
RESULT = 'result'
ERRORS = 'errors'


def get_system(options):
    """Build the system with options and create a structure that keep all axis information

    Args:
        options (dict): Options to build the system. Refers to `System`.

    Returns:
        dict: Result object.
            status (dict): Status object.
                code (str): 200 if all was good. 400 otherwise.\n
                message (str): 'OK' if all was good.
                    'Bad request' if somenting was wrong building objects.
            errors (Optional[dict]): Errors object.
                key (str): field name.\n
                value (str): error.
            result (dict): Result object.
                name (str): System name.\n
                axis (dict): Axis object. With control.\n
                    x (list[float]): X axis.\n
                    y (Optional[list[float]]): Y axis if exists.\n
                    z (Optional[list[float]]): Z axis if exists.\n
                axis_wc (dict): Axis object. Without control.\n
                    x (list[float]): X axis.\n
                    y (Optional[list[float]]): Y axis if exists.\n
                    z (Optional[list[float]]): Z axis if exists.\n
                control (list[float]): Control force.
    """
    response = defaultdict(dict)
    options_ = System.clean_arguments(options)
    system = None
    try:
        system = System(**options_)
    except ValueError as e:  # pylint: disable=invalid-name
        response[STATUS]['code'] = '400'
        response[STATUS]['message'] = 'Bad request'
        response[ERRORS] = e.args[0]

    if system:
        response[STATUS]['code'] = '200'
        response[STATUS]['message'] = 'OK'
        n = options.get('n', 100)  # pylint: disable=invalid-name
        response[RESULT]['name'] = system.get_name()
        response[RESULT]['axis'] = {}
        response[RESULT]['axis_wc'] = {}
        response[RESULT]['axis']['x'] = system.get_serie(System.AXIS_X, n)
        response[RESULT]['axis_wc']['x'] = system.get_serie(System.AXIS_X_WC, n)
        try:
            response[RESULT]['axis']['y'] = system.get_serie(System.AXIS_Y, n)
            response[RESULT]['axis_wc']['y'] = system.get_serie(System.AXIS_Y_WC, n)
            response[RESULT]['axis']['z'] = system.get_serie(System.AXIS_Z, n)
            response[RESULT]['axis_wc']['z'] = system.get_serie(System.AXIS_Z_WC, n)
        except ValueError as e:  # pylint: disable=invalid-name
            pass
        response[RESULT]['control'] = system.get_serie(System.CONTROL_F, n)
    return response


def get_control_and_system(options):
    """Mix between get_control and get_system functions.

    Args:
        options (dict): Options to build the system and run the algoritm.
            Refers to `System` and `Algorithm`.

    Returns:
        dict: Result object.
            status (dict): Status object.
                code (str): 200 if all was good. 400 otherwise.\n
                message (str): 'OK' if all was good.
                    'Bad request' if somenting was wrong building objects.
            errors (Optional[dict]): Errors object.
                key (str): field name.\n
                value (str): error.
            result (dict): Result object.
                name (str): System name.\n
                axis (dict): Axis object. With control.\n
                    x (list[float]): X axis.\n
                    y (Optional[list[float]]): Y axis if exists.\n
                    z (Optional[list[float]]): Z axis if exists.\n
                axis_wc (dict): Axis object. Without control.\n
                    x (list[float]): X axis.\n
                    y (Optional[list[float]]): Y axis if exists.\n
                    z (Optional[list[float]]): Z axis if exists.\n
                control (list[float]): Control force.\n
                k (float): Control value found by the algorithm.\n
                messages (dict): Executions messages of algorithm. See `Algorithm`.
    """
    response = get_control(options)
    if response[STATUS]['code'] == '200':
        if not Algorithm.MSG_NOT_SOLUTION in response[RESULT]['messages']:
            options[System.ARG_K] = response[RESULT]['k']
        # if control code was ok then systen also will be ok
        system = get_system(options)
        # if algorithm didn't find any solution so system won't have control
        response[RESULT].update(system[RESULT])

    return response


def get_control(options):
    """Calculate control values using the system information in options.

    Args:
        options (dict): Options to build the system and run the algoritm.
            Refers to `System` and `Algorithm`.

    Returns:
        dict: Result object.
            status (dict): Status object.
                code (str): 200 if all was good. 400 otherwise.\n
                message (str): 'OK' if all was good.
                    'Bad request' if somenting was wrong building objects.\n
            errors (Optional[dict]): Errors object.
                key (str): field name.\n
                value (str): error.
            result (dict): Result object.
                k (float): Control value found by the algorithm.\n
                messages (dict): Executions messages of algorithm. See `Algorithm`.
    """
    response = get_system(options)
    if response[STATUS]['code'] == '200':
        response = defaultdict(dict)
        system = System(**System.clean_arguments(options))
        options[Algorithm.ARG_SYSTEM] = system
        options_ = Algorithm.clean_arguments(options)
        alg = None
        try:
            alg = Algorithm(**options_)
            alg.search()
            response[STATUS]['code'] = '200'
            response[STATUS]['message'] = 'OK'
            response[RESULT]['k'] = alg.get_result()
            response[RESULT]['messages'] = alg.get_messages()
        except ValueError as e:  # pylint: disable=invalid-name
            response[STATUS]['code'] = '400'
            response[STATUS]['message'] = 'Bad request'
            response[ERRORS] = e.args[0]

    return response
