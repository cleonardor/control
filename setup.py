import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="chaoscontrol",
    version="1.0.0",
    author="Cristian Rios",
    author_email="cristianrios7@outlook.com",
    description="Allows control of some defined chaotic dynamic systems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cleonardor/control",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Framework :: Pytest",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Mathematics"
    ],
    python_requires='>=3.6',
)