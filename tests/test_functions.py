import pytest

from chaoscontrol import functions
from chaoscontrol.algorithm import Algorithm
from chaoscontrol.systems import System


@pytest.fixture
def options():
    options = {
        Algorithm.ARG_INDIVIDUALS: 10,
        Algorithm.ARG_GENERATIONS: 1,
        Algorithm.ARG_EXECUTIONS: 1,
        Algorithm.ARG_PERIOD: 50,
        Algorithm.ARG_ORBITS: 1,
        System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
        System.ARG_X0: 0.1,
        System.ARG_A: 1,
        System.ARG_DELAY: 10,
    }
    return options


def test_get_system_missing_arguments(options):
    del options[System.ARG_A]
    system = functions.get_system(options)
    assert system['status']['code'] == '400'
    assert system['status']['message'] == 'Bad request'
    assert system['errors'] == {System.ARG_A: ['Required argument']}


def test_get_system_unknown_arguments(options):
    options['unknown'] = 'unknown'
    system = functions.get_system(options)
    assert system['status']['code'] == '200'
    assert system['status']['message'] == 'OK'


def test_get_system_default_length(options):
    system = functions.get_system(options)
    assert system['status']['code'] == '200'
    assert system['status']['message'] == 'OK'
    axis = system['result']['axis']
    axis_wc = system['result']['axis_wc']
    length = 100
    assert len(axis['x']) == length
    assert len(axis_wc['x']) == length
    assert len(system['result']['control']) == length


def test_get_system_length(options):
    length = 200
    options['n'] = length
    system = functions.get_system(options)
    assert system['status']['code'] == '200'
    assert system['status']['message'] == 'OK'
    axis = system['result']['axis']
    axis_wc = system['result']['axis_wc']
    assert len(axis['x']) == length
    assert len(axis_wc['x']) == length
    assert len(system['result']['control']) == length


def test_get_system_no_axis(options):
    system = functions.get_system(options)
    assert system['status']['code'] == '200'
    # logistic equation doesn't have y axis
    assert not 'y' in system['result']['axis']
    assert not 'y' in system['result']['axis_wc']


def test_get_system_two_axis(options):
    options[System.ARG_NAME] = System.SYSTEM_HENON
    options[System.ARG_X0] = -0.1
    options[System.ARG_Y0] = 0.1
    options[System.ARG_A] = 1.4
    options[System.ARG_B] = 0.3

    system = functions.get_system(options)
    assert system['status']['code'] == '200'
    assert system['status']['message'] == 'OK'
    axis = system['result']['axis']
    axis_wc = system['result']['axis_wc']
    length = 100
    assert len(axis['x']) == length
    assert len(axis_wc['x']) == length
    assert len(axis['y']) == length
    assert len(axis_wc['y']) == length


def test_get_control_missing_arguments(options):
    del options[Algorithm.ARG_PERIOD]
    control = functions.get_control(options)
    assert control['status']['code'] == '400'
    assert control['status']['message'] == 'Bad request'
    errors = {Algorithm.ARG_PERIOD: ['Required argument', 'Should be greater than 0']}
    assert control['errors'] == errors


def test_get_control_unknown_arguments(options):
    options['unknown'] = 'unknown'
    control = functions.get_control(options)
    assert control['status']['code'] == '200'
    assert control['status']['message'] == 'OK'


def test_get_control_right_values(options):
    control = functions.get_control(options)
    assert control['status']['code'] == '200'
    assert control['status']['message'] == 'OK'
    assert 'result' in control
    assert 'k' in control['result']
    assert 'messages' in control['result']


def test_get_control_and_system(options):
    response = functions.get_control_and_system(options)
    assert response['status']['code'] == '200'
    assert response['status']['message'] == 'OK'
    result = response['result']
    assert 'axis' in result
    assert 'axis_wc' in result
    assert 'control' in result
    assert 'k' in result
    assert 'messages' in result
