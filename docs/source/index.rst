.. Control of Dynamic Chaotic Systems documentation master file, created by
   sphinx-quickstart on Tue Apr  7 13:39:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Control of Dynamic Chaotic Systems's documentation!
==============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
