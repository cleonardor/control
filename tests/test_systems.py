import pytest

from chaoscontrol.systems import System
import chaoscontrol.utilities as utils

@pytest.fixture
def system_arguments():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
        System.ARG_X0: 0.1,
        System.ARG_A: 1,
        System.ARG_DELAY: 10,
    }
    return kwargs

def test_not_valid_arguments(system_arguments):
    unknown = 'unknown'
    system_arguments[unknown] = unknown
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {unknown: ['Argument not valid']}


def test_not_valid_system(system_arguments):
    system_arguments[System.ARG_NAME] = 'unknown'
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_NAME: ["Value not valid"]}


def test_missing_system(system_arguments):
    del system_arguments[System.ARG_NAME]
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_NAME: ["Required argument"]}


def test_missing_x0_initial_values(system_arguments):
    del system_arguments[System.ARG_X0]
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_X0: ["Required argument"]}


def test_missing_y0_initial_values(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_B: 1
    }
    system_arguments.update(kwargs)
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_Y0: ["Required argument"]}


def test_missing_z0_initial_values(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_LORENZ,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
        System.ARG_C: 1,
    }
    system_arguments.update(kwargs)
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_Z0: ["Required argument"]}


def test_missing_a_parameters(system_arguments):
    del system_arguments[System.ARG_A]
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_A: ["Required argument"]}


def test_missing_b_parameters(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.1,
    }
    system_arguments.update(kwargs)
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_B: ["Required argument"]}


def test_missing_c_parameters(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
        System.ARG_D: 1,
    }
    system_arguments.update(kwargs)
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_C: ["Required argument"]}


def test_missing_d_parameters(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
        System.ARG_C: 1,
    }
    system_arguments.update(kwargs)
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_D: ["Required argument"]}


def test_missing_noise_intensity_value(system_arguments):
    system_arguments[System.ARG_NOISE] = True
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_NOISE_INTENSITY: ["Required argument"]}


def test_noise_default_values(system_arguments):
    system = System(**system_arguments)
    assert not getattr(system, utils.attrp(System.ARG_NOISE))


def test_noise_default_values2(system_arguments):
    kwargs = {
        System.ARG_NOISE: True,
        System.ARG_NOISE_INTENSITY: 0.1,
        System.ARG_DELAY: 10,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert getattr(system, utils.attrp(System.ARG_NOISE_MEAN)) == 0
    assert getattr(system, utils.attrp(System.ARG_NOISE_STD)) == 1


def test_missing_control_values(system_arguments):
    del system_arguments[System.ARG_DELAY]
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_DELAY: ["Required argument"]}


def test_wrong_control_values(system_arguments):
    system_arguments[System.ARG_DELAY] = 1
    # delay should be > 1
    with pytest.raises(ValueError) as excinfo:
        System(**system_arguments)
    assert excinfo.value.args[0] == {System.ARG_DELAY: ["Should be greater than 1"]}


def test_default_control_values(system_arguments):
    system = System(**system_arguments)
    assert getattr(system, utils.attrp(System.ARG_K)) == 0


def test_missing_multiple_values():
    with pytest.raises(ValueError) as excinfo:
        System()
    assert excinfo.value.args[0] == {
        System.ARG_NAME: ["Required argument"],
        System.ARG_X0: ["Required argument"],
        System.ARG_A: ["Required argument"],
        System.ARG_DELAY: ["Required argument"]
    }


def test_init(system_arguments):
    kwargs = {
        System.ARG_NOISE: True,
        System.ARG_NOISE_INTENSITY: 0.1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert getattr(system, utils.attrp(System.ARG_NAME)) == System.SYSTEM_LOGISTIC_EQUATION
    assert getattr(system, utils.attrp(System.ARG_X0)) == 0.1
    assert getattr(system, utils.attrp(System.ARG_A)) == 1
    assert getattr(system, utils.attrp(System.ARG_NOISE))
    assert getattr(system, utils.attrp(System.ARG_NOISE_INTENSITY)) == 0.1
    assert getattr(system, utils.attrp(System.ARG_DELAY)) == 10


def _test_get_serie_axis_fail(system, axis):
    with pytest.raises(ValueError, match=r"{}: Axis not valid".format(axis)):
        system.get_serie(axis, 0)

def test_get_serie_wrong_axis(system_arguments):
    system = System(**system_arguments)
    # logistic equation only have x and x_wc axis
    _test_get_serie_axis_fail(system, System.AXIS_Y)
    _test_get_serie_axis_fail(system, System.AXIS_Y_WC)


def test_get_serie_wrong_axis2(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    # henon system only has x, x_wc, y and y_wc axis
    _test_get_serie_axis_fail(system, System.AXIS_Z)
    _test_get_serie_axis_fail(system, System.AXIS_Z_WC)


def test_get_serie_bad_axis(system_arguments):
    system = System(**system_arguments)
    _test_get_serie_axis_fail(system, 'w')


def test_set_n(system_arguments):
    system = System(**system_arguments)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    N = 50
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N


def test_set_k(system_arguments):
    system = System(**system_arguments)
    assert getattr(system, utils.attrp(System.ARG_K)) == 0
    K = 1234
    system.set_k(K)
    assert getattr(system, utils.attrp(System.ARG_K)) == K


def test_get_all_axis_logistic():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
        System.ARG_X0: 0.1,
        System.ARG_A: 3.9,
        System.ARG_DELAY: 10,
    }
    system = System(**kwargs)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    f = system.get_serie(System.CONTROL_F, N)
    assert len(f) == N


def test_get_all_axis_henon():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_X0: -0.1,
        System.ARG_Y0: 0.1,
        System.ARG_A: 1.4,
        System.ARG_B: 0.3,
        System.ARG_DELAY: 10,
    }
    system = System(**kwargs)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    y = system.get_serie(System.AXIS_Y, N)
    assert len(y) == N
    x_wc = system.get_serie(System.AXIS_X_WC, N)
    assert len(x_wc) == N
    y_wc = system.get_serie(System.AXIS_Y_WC, N)
    assert len(y_wc) == N
    f = system.get_serie(System.CONTROL_F, N)
    assert len(f) == N


def test_get_all_axis_tinkerbel():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_TINKERBELL,
        System.ARG_X0: 0.1,
        System.ARG_Y0: 0.1,
        System.ARG_A: 0.5,
        System.ARG_B: -0.6,
        System.ARG_C: 2.2,
        System.ARG_D: 0.5,
        System.ARG_DELAY: 10,
    }
    system = System(**kwargs)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    y = system.get_serie(System.AXIS_Y, N)
    assert len(y) == N
    x_wc = system.get_serie(System.AXIS_X_WC, N)
    assert len(x_wc) == N
    y_wc = system.get_serie(System.AXIS_Y_WC, N)
    assert len(y_wc) == N
    f = system.get_serie(System.CONTROL_F, N)
    assert len(f) == N


def test_get_all_axis_ikeda():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_X0: 0.1,
        System.ARG_Y0: 0.1,
        System.ARG_A: 1,
        System.ARG_B: 0.9,
        System.ARG_C: 0.4,
        System.ARG_D: 6,
        System.ARG_DELAY: 10,
    }
    system = System(**kwargs)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    y = system.get_serie(System.AXIS_Y, N)
    assert len(y) == N
    x_wc = system.get_serie(System.AXIS_X_WC, N)
    assert len(x_wc) == N
    y_wc = system.get_serie(System.AXIS_Y_WC, N)
    assert len(y_wc) == N
    f = system.get_serie(System.CONTROL_F, N)
    assert len(f) == N


def test_get_all_axis_with_noise():
    kwargs = {
        System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
        System.ARG_X0: 0.1,
        System.ARG_A: 3.9,
        System.ARG_DELAY: 10,
        System.ARG_NOISE: True,
        System.ARG_NOISE_INTENSITY: 0.1
    }
    system = System(**kwargs)
    N = 100
    x = system.get_serie(System.AXIS_X, N)
    assert len(x) == N
    f = system.get_serie(System.CONTROL_F, N)
    assert len(f) == N


def test_get_delay(system_arguments):
    system = System(**system_arguments)
    assert system.get_delay() == 10


def test_get_x0(system_arguments):
    system = System(**system_arguments)
    assert system.get_x0() == 0.1


def test_get_y0(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert system.get_y0() == 0.1


def test_get_y0_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.get_y0() == 0.1
    msg = "{} has not attribute y0".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_get_z0(system_arguments):
    # there are not systems with three axis
    pass


def test_get_z0_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.get_z0() == 0.1
    msg = "{} has not attribute z0".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_get_a(system_arguments):
    system = System(**system_arguments)
    assert system.get_a() == 1


def test_get_b(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.2,
        System.ARG_B: 1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert system.get_b() == 1


def test_get_b_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.get_b()
    msg = "{} has not attribute b".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_get_c(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 0.9,
        System.ARG_C: 0.4,
        System.ARG_D: 6,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert system.get_c() == 0.4


def test_get_c_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.get_c()
    msg = "{} has not attribute c".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_get_d(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 0.9,
        System.ARG_C: 0.4,
        System.ARG_D: 6,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert system.get_d() == 6


def test_get_d_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.get_d()
    msg = "{} has not attribute d".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_get_noise(system_arguments):
    system = System(**system_arguments)
    assert not system.get_noise()


def test_get_noise_intensity(system_arguments):
    system = System(**system_arguments)
    assert system.get_noise_intensity() == 0
    kwargs = {
        System.ARG_NOISE: True,
        System.ARG_NOISE_INTENSITY: 0.2
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    assert system.get_noise_intensity() == 0.2
    

def test_get_noise_mean(system_arguments):
    system = System(**system_arguments)
    assert system.get_noise_mean() == 0


def test_get_noise_std(system_arguments):
    system = System(**system_arguments)
    assert system.get_noise_std() == 1


def test_get_k(system_arguments):
    system = System(**system_arguments)
    assert system.get_k() == 0.0
    system_arguments[System.ARG_K] = 1
    system = System(**system_arguments)
    assert system.get_k() == 1


def test_set_x0(system_arguments):
    system = System(**system_arguments)
    system.set_x0(3)
    assert system.get_x0() == 3


def test_set_y0(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.1,
        System.ARG_B: 1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    system.set_y0(3)
    assert system.get_y0() == 3


def test_set_y0_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.set_y0(3)
    msg = "{} has not attribute y0".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_set_z0(system_arguments):
    # there are not systems with three axis
    pass


def test_set_z0_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.set_z0(3)
    msg = "{} has not attribute z0".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_set_a(system_arguments):
    system = System(**system_arguments)
    system.set_a(3)
    assert system.get_a() == 3


def test_set_b(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_HENON,
        System.ARG_Y0: 0.2,
        System.ARG_B: 1,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    system.set_b(3)
    assert system.get_b() == 3


def test_set_b_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.set_b(3)
    msg = "{} has not attribute b".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_set_c(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 0.9,
        System.ARG_C: 0.4,
        System.ARG_D: 6,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    system.set_c(3)
    assert system.get_c() == 3


def test_set_c_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.set_c(3)
    msg = "{} has not attribute c".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_set_d(system_arguments):
    kwargs = {
        System.ARG_NAME: System.SYSTEM_IKEDA,
        System.ARG_Y0: 0.1,
        System.ARG_B: 0.9,
        System.ARG_C: 0.4,
        System.ARG_D: 6,
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    system.set_d(3)
    assert system.get_d() == 3


def test_set_d_wrong_system(system_arguments):
    system = System(**system_arguments)
    with pytest.raises(AttributeError) as excinfo:
        system.set_d(3)
    msg = "{} has not attribute d".format(System.SYSTEM_LOGISTIC_EQUATION)
    assert excinfo.value.args[0] == msg


def test_set_noise(system_arguments):
    system = System(**system_arguments)
    system.set_noise(True)
    assert system.get_noise()
    assert system.get_noise_intensity() == 0


def test_set_noise_intensity(system_arguments):
    kwargs = {
        System.ARG_NOISE: True,
        System.ARG_NOISE_INTENSITY: 0.2
    }
    system_arguments.update(kwargs)
    system = System(**system_arguments)
    system.set_noise_intensity(3)
    assert system.get_noise_intensity() == 3
    

def test_set_noise_mean(system_arguments):
    system = System(**system_arguments)
    system.set_noise_mean(3)
    assert system.get_noise_mean() == 3


def test_set_noise_std(system_arguments):
    system = System(**system_arguments)
    system.set_noise_std(3)
    assert system.get_noise_std() == 3

