# Control of Chaotic Dynamic Systems

This library allows make control of some defined chaotic dynamic systems. It uses an evolutionary algorithm to find the k value to apply a variation of Positive Feedback Control over discrete chaotic dinamy systems like Logistic Equation, Ikeda, Tinkerbell or Henon systems.

## Installation

This package could be installed using the [Python Package Index (PyPI)](https://pypi.org/) or cloning the repository.

### Using PyPI
```bash
$ pip install chaoscontrol
```
### Cloning repository
```bash
$ git clone https://gitlab.com/cleonardor/control chaoscontrol
```

## Usage

Those are some usage examples, there are also in Documentation. Please refere to it for more information about methods, types and usage.
```python
from chaoscontrol import System
options = {
    System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
    System.ARG_X0: 0.1,
    System.ARG_A: 1,
    System.ARG_NOISE: False,
    System.ARG_DELAY: 10,
}
system = System(**options)  # build the system object
serie_x = system.get_serie(System.AXIS_X, 100)  # get a time serie of lenght 100

# You can also use the default defined values
from chaoscontrol import options_logistic_equation
system = System(**options_logistic_equation)
```
```python
from chaoscontrol import (
    Algorithm,
    system_logistic_equation,
)
options = {
    'individuals': 100,
    'generations': 100,
    'executions': 10,
    'period': 50,
    'orbits': 1,
}
algorithm = Algorithm(
    system=system_logistic_equation,  # this is a predefine system with default values
    **options
)
algorithm.search()  # execute the algorithm
algorithm.get_result()  # get the control value, if exist

# You can also use the default defined values
from chaoscontrol import options_algorithm
algorithm = Algorithm(system=system_logistic_equation, **options_algorithm)
```
```python
# For a final use you can use get_control, get_system or get_control_and_system functions.
# See documentation for more details.
from chaoscontrol import (
    Algorithm,
    System,
    get_control_and_system,
)
options = {
    Algorithm.ARG_INDIVIDUALS: 10,
    Algorithm.ARG_GENERATIONS: 1,
    Algorithm.ARG_EXECUTIONS: 1,
    Algorithm.ARG_PERIOD: 50,
    Algorithm.ARG_ORBITS: 1,
    System.ARG_NAME: System.SYSTEM_LOGISTIC_EQUATION,
    System.ARG_X0: 0.1,
    System.ARG_A: 1,
    System.ARG_DELAY: 10,
}
get_control_and_system(options)  # utility function
```

## Develop Environment
You can use [docker-compose](https://docs.docker.com/compose/) to build a develop environment if you need to run tests, linter or update documentation.
```bash
$ docker-compose up -d
```
When finish don't forget down the container.
```bash
$ docker-compose down
```
### Run tests and linter
```bash
$ docker-compose exec control sh local_build.sh
```
### Build documentation
```bash
$ docker-compose exec control sh docs_build.sh
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests and docs as appropriate.

## License
[GPL3](http://www.gnu.org/licenses/gpl-3.0.html)